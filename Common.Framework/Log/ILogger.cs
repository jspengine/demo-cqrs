﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Framework.Logger
{
    public interface ILogger
    {
        void WriteError(int errorCode, string message);
        void WriteError(int errorCode, Exception ex);
        void WriteWarning(int warningCode, string message);
    }
}
