﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Framework.Logger
{
    public class Logger : ILogger
    {
        private TraceSource trace;

        public Logger(string name)
        {
            trace = new TraceSource(name);
        }

        public void WriteError(int errorCode, string message)
        {
            trace.TraceEvent(TraceEventType.Error, errorCode, message);
        }

        public void WriteError(int errorCode, Exception ex)
        {
            trace.TraceData(TraceEventType.Error, errorCode, ex);
        }

        public void WriteWarning(int warningCode, string message)
        {
            trace.TraceEvent(TraceEventType.Warning, warningCode, message);
        }
    }


}
