﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;

namespace Common.Framework.Application
{
    public class BaseApplicationContext : IApplicationContext
    {
        public virtual string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings["enquete"].ConnectionString;
        }

        public IApplicationContext CurrentContext
        {
            get { return (IApplicationContext)HttpContext.Current.Items["ApplicationContext"]; }
        }

    }
}
