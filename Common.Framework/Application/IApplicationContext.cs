﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Framework.Application
{
    public interface IApplicationContext
    {
        string GetConnectionString(string name);
        IApplicationContext CurrentContext { get; }
    }
}
