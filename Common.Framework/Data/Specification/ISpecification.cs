﻿using System;
using System.Linq.Expressions;

namespace Common.Framework.Data.Specification
{
    public interface ISpecification<TEntity> where TEntity :class
    {
        Expression<Func<TEntity, bool>> SatisfiedBy();
    }
}
