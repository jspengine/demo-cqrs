﻿namespace Common.Framework.Data.Repository
{
    public interface ISorting
    {
        string ColumnName { get; set; }
        bool Ascending { get; set; }
    }
}
