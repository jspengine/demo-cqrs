﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Framework.Command
{
    public class CommandDispacther : ICommandDispatcher
    {
        protected static IUnityContainer container;
        public CommandDispacther(IUnityContainer _container)
        {
            if (_container == null) throw new ArgumentNullException("Container não pode ser nulo");
            container = _container;
        }
        public void Dispatcher<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = container.Resolve<ICommandHandler<TCommand>>("CommandHandler");
            handler.Handle(command);
        }
    }
}
