﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Framework.Command
{
    public interface ICommandDispatcher 
    {
        /// <summary>
        /// Dispatcher the command to its handler
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        void Dispatcher<TCommand>(TCommand command) where TCommand : ICommand;

    }
}
