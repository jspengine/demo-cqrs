﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Framework.Command
{
    public interface ICommand
    {
       Guid Oid { get; set; }
    }
}
