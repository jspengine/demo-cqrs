﻿namespace Common.Framework.Cache
{
    public interface ICaching
    {
        /// <summary>
        /// Insere valor em cache usando os parâmetros apropriados
        /// </summary>
        /// <typeparam name="T">Tipo Objeto Cache</typeparam>
        /// <param name="key">Nome item</param>
        void Add<T>(T @object, string key);

        /// <summary>
        /// Insere valor em cache usando os parâmetros apropriados
        /// </summary>
        /// <typeparam name="T">Tipo Objeto Cache</typeparam>
        /// <param name="key">Nome item</param>
        /// <param name="timeout">Total em minutos</param>
        void Add<T>(T @object, string key, double timeout);

        /// <summary>
        /// Remove item do cache
        /// </summary>
        /// <param name="key">Nome item</param>
        void Remove(string key);

        /// <summary>
        /// Verifica existencia do item no chace
        /// </summary>
        /// <param name="key">Nome item</param>
        /// <returns></returns>
        bool IsNull(string key);

        /// <summary>
        /// Recuperar item do Cache
        /// </summary>
        /// <typeparam name="T">Tipo Objeto</typeparam>
        /// <param name="key">Nome item</param>
        /// <returns>Objeto tipado</returns>
        T Get<T>(string key);

        string[] AllKeys();
    }
}
