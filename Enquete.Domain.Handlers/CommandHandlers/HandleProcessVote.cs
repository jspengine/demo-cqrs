﻿using Common.Framework.Command;
using Enquete.Domain.Foundation.Contracts.Commands;
using Enquete.Domain.Foundation.Contracts.Repository;
using Enquete.Domain.Foundation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Handlers.CommandHandlers
{
    public class HandlerProcessVote :
        ICommandHandler<ProcessVote>,
        ICommandHandler<AmountOfVotesUpdate> 
    {
        protected IVotingRepository votingRepository;
        protected IParticipantRepository participantRepository;
        public HandlerProcessVote(IVotingRepository _votingRepository, IParticipantRepository _particpantRepository)
        {
            votingRepository = _votingRepository;
            participantRepository = _particpantRepository;
        }
        public void Handle(ProcessVote command)
        {
            var voting = new Voting()
            {
                Oid = command.Oid,
                ParticipantId = command.ParticipantId,
                DateOfVote = DateTime.Now
            };

            votingRepository.Add(voting);
            votingRepository.UnitOfWork.Commit();
        }

        public void Handle(AmountOfVotesUpdate command)
        {
            var participant = participantRepository.Get(command.Oid);
            participant.AmountOfVotes += 1;
            participantRepository.Modify(participant);
            participantRepository.UnitOfWork.Commit();
            
        }
    }
}
