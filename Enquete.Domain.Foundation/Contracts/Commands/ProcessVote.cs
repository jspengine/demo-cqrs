﻿using Common.Framework.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Foundation.Contracts.Commands
{
    public class ProcessVote : ICommand
    {
        public ProcessVote()
        {
            Oid = Guid.NewGuid();
        }
        public Guid Oid {get; set;}
        public Guid ParticipantId { get; set; }
    }
}
