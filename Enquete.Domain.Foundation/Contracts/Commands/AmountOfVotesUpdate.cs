﻿using Common.Framework.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Foundation.Contracts.Commands
{
    public class AmountOfVotesUpdate : ICommand
    {

        public AmountOfVotesUpdate()
        {
            Oid = Guid.NewGuid();
        }
        public Guid Oid { get; set; }
        
    }
}
