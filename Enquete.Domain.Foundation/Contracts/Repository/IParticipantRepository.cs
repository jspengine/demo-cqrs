﻿using Common.Framework.Data.Repository;
using Enquete.Domain.Foundation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Foundation.Contracts.Repository
{
    public interface IParticipantRepository : IRepository<Participant, Guid>
    {
    }
}
