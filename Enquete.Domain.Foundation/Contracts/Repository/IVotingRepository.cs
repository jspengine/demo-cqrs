﻿using Common.Framework.Data.Repository;
using Enquete.Domain.Foundation.Entities;
using System;

namespace Enquete.Domain.Foundation.Contracts.Repository
{
    public interface IVotingRepository : IRepository<Voting, Guid>
    {
    }
}
