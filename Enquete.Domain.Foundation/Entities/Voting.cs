﻿using Common.Framework.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Foundation.Entities
{
    public class Voting : BaseEntity
    {
        public Guid Oid { get; set; }
        public virtual Guid  ParticipantId { get; set; }
        public DateTime DateOfVote { get; set; }

    }
}
