﻿using Common.Framework.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Foundation.Entities
{
    public class Participant : BaseEntity
    {
        public Guid Oid { get; set; }
        public string Name { get; set; }
        public string Power { get; set; }
        public int AmountOfVotes { get; set; }

    }
}
