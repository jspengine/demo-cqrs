﻿using Common.Framework.Application;
using Common.Framework.Cache;
using Common.Framework.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enquete.Api
{
    public class ApplicationContext : BaseApplicationContext
    {

        public ApplicationContext() 
        {
        }

        
        public override string GetConnectionString(string name)
        {
            var conn = System.Configuration.ConfigurationManager.ConnectionStrings["enquete"].ConnectionString;
            if (String.IsNullOrEmpty(conn))
                throw new ConnectionStringIsNotConfigured("Connection String is not configured");

            return conn;
        }
    }
}