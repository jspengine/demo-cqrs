﻿using AutoMapper;
using Enquete.Api.Mappers;

namespace Enquete.Api
{
    public class MapperConfig
    {
        public static void Configure() 
        {
            Mapper.AddProfile(new ParticipantMapperProfile());
        }
    }
}