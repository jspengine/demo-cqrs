using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Common.Framework.Cache;
using Common.Framework.Logger;
using Common.Framework.Application;
using Common.Framework.Data.Repository;
using Enquete.Integration;
using Enquete.Domain.Integration.Repositories;
using Enquete.Domain.Foundation.Contracts.Repository;
using System.Web.Http.Dependencies;
using System.Web.Http;
using Common.Framework.Command;
using Enquete.Domain.Handlers.CommandHandlers;
using System.Reflection;

namespace Enquete.Api
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {

        protected HttpConfiguration configuration;
        public UnityConfig(HttpConfiguration _configuration)
        {
            configuration = _configuration;
        }

        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);    
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType<ICaching, MemoryCacheManager>();
            container.RegisterType<ILogger>( new InjectionFactory(
                f => {
                    return new Logger("EnqueteTraceSource");
            }));
            container.RegisterType<IApplicationContext, ApplicationContext>();
            container.RegisterType<IDbContextUnitOfWork>(new InjectionFactory(
                f => {
                    return new DbContextUnitOfWork(new ApplicationContext().GetConnectionString("enquete")); 
            }));

            //Repositories shoud be configured in child container.
            container.RegisterType<IVotingRepository, VotingRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IParticipantRepository, ParticipantRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ICommandDispatcher, CommandDispacther>();

            container.RegisterType(typeof(ICommandHandler<>), typeof(HandlerProcessVote), "CommandHandler", 
                new InjectionFactory( f => {
                    var votingRepository = container.Resolve<IVotingRepository>();
                    var partipantRepository = container.Resolve<IParticipantRepository>();
                    return new HandlerProcessVote(votingRepository, partipantRepository);
            }));
            
        }
    }


    public class WebApiUnityResolver : IDependencyResolver
    {
        protected IUnityContainer container;
        public WebApiUnityResolver(IUnityContainer _container)
        {
            if (_container == null)
                throw new ArgumentException("Container n�o pode ser nulo");

            this.container = _container;
        }


        public IDependencyScope BeginScope()
        {
            var child = container.CreateChildContainer();
            return new WebApiUnityResolver(child);
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return container.Resolve(serviceType);
            }
            catch (ResolutionFailedException)
            {
                return null;
            }
        }

        public System.Collections.Generic.IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return container.ResolveAll(serviceType);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void Dispose()
        {
            container.Dispose();
        }
    }


}
