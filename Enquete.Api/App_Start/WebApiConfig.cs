﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Enquete.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Configuring the response formatter in web api to Json
            var formatters = GlobalConfiguration.Configuration.Formatters;
            formatters.Remove(formatters.XmlFormatter);

            //Configure container for Web API;
            config.DependencyResolver = new WebApiUnityResolver(UnityConfig.GetConfiguredContainer());
        }
    }
}
