﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Enquete.Api.Model
{
    public class ParticipantViewModel
    {
        public Guid Oid { get; set; }
        [Required]
        public string Name { get; set; }
        public string Power { get; set; }
        public int AmountOfVotes { get; set; }

        public string ImageUrl { get; set; }
    }
}
