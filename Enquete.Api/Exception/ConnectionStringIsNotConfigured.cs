﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Enquete.Api
{
    class ConnectionStringIsNotConfigured : Exception
    {
        public ConnectionStringIsNotConfigured(string message) : base(message)
        {
        }
    }
}
