﻿using AutoMapper;
using Enquete.Api.Model;
using Enquete.Domain.Foundation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enquete.Api.Mappers
{
    public class ParticipantMapperProfile : Profile
    {
        public ParticipantMapperProfile()
        {
            Mapper.CreateMap<Participant, ParticipantViewModel>()
                .ForMember(entity => entity.Oid, model => model.MapFrom(src => src.Oid))
                .ForMember(entity => entity.Name, model => model.MapFrom(src => src.Name))
                .ForMember(entity => entity.Power, model => model.MapFrom(src => src.Power))
                .ForMember(entity => entity.Oid, model => model.MapFrom(src => src.AmountOfVotes));
        }
    }
}