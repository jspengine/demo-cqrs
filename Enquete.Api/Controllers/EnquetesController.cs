﻿using AutoMapper;
using Common.Framework.Data.Specification;
using Enquete.Api.Model;
using Enquete.Domain.Foundation.Contracts.Repository;
using Enquete.Domain.Foundation.Entities;
using System.Web.Http;
using System.Linq;
using System;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.Web;
using Common.Framework.Command;
using Enquete.Domain.Foundation.Contracts.Commands;




namespace Enquete.Api.Controllers
{
    [RoutePrefix("api/enquetes")]
    public class EnquetesController : BaseController
    {


        protected IParticipantRepository participantRepository;
        private readonly ICommandDispatcher commandDispatcher;

        public EnquetesController(IParticipantRepository _particpantRepository, ICommandDispatcher _commandDispatcher)
        {
            commandDispatcher = _commandDispatcher;
            participantRepository = _particpantRepository;
        }


        [Route("vote/{id}"), HttpPost, AllowAnonymous]
        public IHttpActionResult ProcessVote(Guid id)
        {
            //Para processar o voto é preciso enviar um comando de computar o voto.
            //Computar o voto significa que deverá ser salvo um registro na base voting, 
            //e depois deverá ser atualizado o contador para cada participante da enquete

            var participant = participantRepository.Get(id);

            commandDispatcher.Dispatcher<ProcessVote>(new ProcessVote()
            {
                ParticipantId = participant.Oid
            });

            commandDispatcher.Dispatcher<AmountOfVotesUpdate>(new AmountOfVotesUpdate()
            {
                Oid = participant.Oid
            });

            //participant.AmountOfVotes += 1;
            //participantRepository.Modify(participant);


            //votingRepository.Add(new Voting()
            //{
            //    Oid = Guid.NewGuid(),
            //    ParticipantId = participant.Oid,
            //    DateOfVote = DateTime.Now
            //});

            //votingRepository.UnitOfWork.Commit();
            //participantRepository.UnitOfWork.Commit();

            return Ok();

        }

        [Route("Participants"), HttpGet, AllowAnonymous]
        public IEnumerable<ParticipantViewModel> GetParticipants()
        {
            return participantRepository.GetAll().Select(p => new ParticipantViewModel()
            {
                Oid = p.Oid,
                Name = p.Name,
                Power = p.Power,
                AmountOfVotes = p.AmountOfVotes,
                ImageUrl = Url.Content(string.Format("~/Content/Images/{0}.png", p.Name.ToLower()))
            }).ToList();
        }
    }
}