﻿using Common.Framework.Data.Repository;
using Common.Framework.Logger;
using Enquete.Api;
using Microsoft.Practices.Unity;
using System;
using System.Web.Http;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Threading;
using System.Web;
using System.Net.Http;

namespace Enquete.Api.Controllers
{
    public abstract class BaseController : ApiController
    {
        protected ILogger Logger
        {
            get
            {
                return UnityConfig.GetConfiguredContainer().Resolve<ILogger>();
            }
        }

        public BaseController()
        {
        }

        public override async Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
        {
            var context = new ApplicationContext();
            HttpContext.Current.Items["ApplicationContext"] = context;
            return await base.ExecuteAsync(controllerContext, cancellationToken);
        }

        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            try
            {
                var dbContextUnitOfWork = UnityConfig.GetConfiguredContainer().Resolve<IDbContextUnitOfWork>();
                var pendingMigrations = dbContextUnitOfWork.GetPendingMigrations();
                if (pendingMigrations.Any())
                    dbContextUnitOfWork.UpdateDatabase();
            }
            catch (Exception ex)
            {
                Logger.WriteError(1, ex);
            }

            base.Initialize(controllerContext);
        }
    }
}