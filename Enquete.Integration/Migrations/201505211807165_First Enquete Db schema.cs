namespace Enquete.Domain.Integration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstEnqueteDbschema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Voting",
                c => new
                    {
                        Oid = c.Guid(nullable: false),
                        ParticipantId = c.Guid(nullable: false),
                        DateOfVotes = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Oid);
            
            CreateTable(
                "dbo.Participant",
                c => new
                    {
                        Oid = c.Guid(nullable: false),
                        Name = c.String(),
                        Power = c.String(),
                        AmountOfvotes = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Oid);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Participant");
            DropTable("dbo.Voting");
        }
    }
}
