﻿namespace Enquete.Domain.Integration.Migrations
{
    using Enquete.Domain.Foundation.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Enquete.Integration.DbContextUnitOfWork>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(Enquete.Integration.DbContextUnitOfWork context)
        {

            if (!context.GetPendingMigrations().Any()) return;
            //Preenchendo a tabela de participantes com os dados iniciais dos particpantes da enquete
            context.Set<Participant>().AddOrUpdate(
                p => p.Name, 
                    new Participant() { Oid = Guid.NewGuid(), Name = "Goku", Power = "Came hame haaaaaa", AmountOfVotes = 0},
                    new Participant() { Oid = Guid.NewGuid(), Name = "Yusuke Urameshe", Power = "Leigannnnn", AmountOfVotes = 0}
                );
        }
    }
}
