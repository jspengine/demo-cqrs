﻿using Common.Framework.Data.Repository;
using Enquete.Domain.Foundation.Contracts.Repository;
using Enquete.Domain.Foundation.Entities;
using System;

namespace Enquete.Domain.Integration.Repositories
{
    public class VotingRepository : Repository<Voting, Guid>, IVotingRepository
    {
        protected readonly IDbContextUnitOfWork _unitOfWork;
        public VotingRepository(IDbContextUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
