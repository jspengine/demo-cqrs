﻿using Common.Framework.Data.Repository;
using Enquete.Domain.Foundation.Contracts.Repository;
using Enquete.Domain.Foundation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Integration.Repositories
{
    public class ParticipantRepository : Repository<Participant, Guid>, IParticipantRepository
    {

        protected readonly IDbContextUnitOfWork _unitOfWork;
        public ParticipantRepository(IDbContextUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
