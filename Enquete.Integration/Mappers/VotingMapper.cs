﻿using Enquete.Domain.Foundation.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Integration.Mappers
{
    public class VotingMapper : EntityTypeConfiguration<Voting>
    {
        public VotingMapper()
        {
            this.HasKey(o => o.Oid);
            this.Property(o => o.ParticipantId).HasColumnName("ParticipantId").IsRequired();
            this.Property(o => o.DateOfVote).HasColumnName("DateOfVotes");
            this.ToTable("Voting");
        }
    }
}
