﻿using Enquete.Domain.Foundation.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete.Domain.Integration.Mappers
{
    public class ParticipantMapper : EntityTypeConfiguration<Participant>
    {
        public ParticipantMapper()
        {
            this.HasKey(o => o.Oid);
            this.Property(o => o.Power).HasColumnName("Power");
            this.Property(o => o.Name).HasColumnName("Name");
            this.Property(o => o.AmountOfVotes).HasColumnName("AmountOfvotes");
            this.ToTable("Participant");
        }
    }
}
