﻿var enqueteApp = angular.module('enqueteApp', []);

enqueteApp.service('httpClientService', function ($http) {

    return {
        getParticipants: function (url) {
            return $http.get(url).then(
                         function (result) {
                             return result.data;
                         });
        },
        postVote: function (url) {
            $http.post(url);
        }
    };

});

enqueteApp.controller('EnqueteController', function ($scope, httpClientService) {

    var baseApiUrl = "http://localhost/Enquete.Api/";
    var getParticipantsApiUrl = "api/enquetes/participants";
    var postProcessVoteApiUrl = "api/enquetes/vote/";

    httpClientService.getParticipants(baseApiUrl + getParticipantsApiUrl)
                    .then(function (gladiators) {
                        $scope.gladiators = gladiators;
                    });

    $scope.gladiator = {};

    $scope.setGladiator = function (gladiator) {
        $scope.gladiator = gladiator;
    };

    $scope.sendProcessVoteCommand = function (gladiator) {
        httpClientService.postVote(baseApiUrl + postProcessVoteApiUrl + $scope.gladiator.Oid);
        $("#confirm").modal('hide');
        Rebind();
    };

    function Rebind() {
        httpClientService.getParticipants(baseApiUrl + getParticipantsApiUrl)
                    .then(function (gladiators) {
                        $scope.gladiators = gladiators;
                        $scope.$apply();
                    });
    }
});