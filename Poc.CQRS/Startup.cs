﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Poc.CQRS.Startup))]
namespace Poc.CQRS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
